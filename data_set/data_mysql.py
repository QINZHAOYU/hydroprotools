import re
import mysql.connector as con
import toolkits.filetools as tool

import asyncio
from aiohttp import TCPConnector, ClientSession


class Demo(object):
    def __init__(self):
        pass

    def demo1(self):
        # 创建链接数据库
        config = {
            'host': 'localhost',
            'user': 'root',
            'password': '2020',
            'port': 3306,
            'database': 'test',
            'charset': 'utf8'
        }
        try:
            cnn = con.connect(**config)
        except con.Error as e:
            print('数据库链接失败！', str(e))
        else:
            print("数据库链接成功！")
            return cnn

    def demo2(self):
        cnn = self.demo1()

        # 建表,注意``不是''
        sql_create_table = "CREATE TABLE IF NOT EXISTS `student` " \
                           "(`id` INT(10) NOT NULL AUTO_INCREMENT, " \
                           "`name` VARCHAR(10) DEFAULT NULL," \
                           "`age` INT(3) DEFAULT NULL, " \
                           "PRIMARY KEY(`id`)) " \
                           "ENGINE=MyISAM DEFAULT CHARSET=utf8"

        # 获取执行的权限，利用数据库连接的返回，调用cursor方法获取一个标记位，再去操作数据库
        # buffered=True会把结果集保存到本地并一次性返回，这样可以提高性能
        cursor = cnn.cursor(buffered=True)
        try:
            cursor.execute(sql_create_table)
        except con.Error as e:
            print("创建表失败", str(e))
        else:
            print("创建表成功！")
            return cnn, cursor

    def demo3(self):
        # 插入数据库
        cnn, cursor = self.demo2()

        try:
            # 第一种： 直接字符串插入方式
            # 表中id是自增长，故不用传id; 此处''不是``
            sql_insert1 = "insert into student(name, age) values ('tom', 20)"
            cursor.execute(sql_insert1)

            # 第二种： 元组插入方式
            # 此处%s是占位符，不是格式化字符串
            sql_insert2 = "insert into student(name, age) values (%s, %s)"
            data = ('jack', 18)
            cursor.execute(sql_insert2, data)

            # 当数据库引擎为innodb，执行完需要commit进行事务提交
            # 目前使用的是myisam引擎，不用执行commit
            # cnn.commit()
            # cursor.execute('commit')

            # 第三种： 一次插入多条数据
            sql_insert3 = "insert into student(name, age) values (%s, %s)"
            datas = [
                ('alan', 21),
                ('rose', 19),
                ('siri', 10)
            ]
            cursor.executemany(sql_insert3, datas)

        except con.Error as e:
            print("插入数据报错", str(e))
        else:
            print("插入数据成功！")
        finally:
            cursor.close()   # 关闭标记位
            cnn.close()      # 关闭数据库链接

    def demo4(self):
        # 查询数据
        cnn = self.demo1()
        cursor = cnn.cursor(buffered=True)

        try:
            # 第一种查询方式
            sql_query1 = "select id,name from student where age > %s"
            cursor.execute(sql_query1, (10,))   # 固定格式
            values = cursor.fetchall()          # 所有符合条件的数据都赋值给values
            print("查询结果：  ", values)

            # 第二种查询方式
            sql_query2 = "select * from student"
            cursor.execute(sql_query2)
            result1 = cursor.fetchone()
            print("一条数据", result1)
            result2 = cursor.fetchone()[0]      # [0]表示只返回第一个字段
            print("第一个字段", result2)
            result3 = cursor.fetchmany(2)       # 返回2行数据
            print("两条数据", result3)

        except con.Error as e:
            print("查询数据报错", str(e))
        else:
            print("数据查询成功！")
        finally:
            cursor.close()
            cnn.close()

    def demo5(self):
        # 删除
        cnn = self.demo1()
        cursor = cnn.cursor(buffered=True)

        try:
            sql_delete = "delete from student where name = %s and age < %s"
            data = ('jack', 18)
            cursor.execute(sql_delete, data)

        except con.Error as e:
            print("删除失败", str(e))
        else:
            print("删除成功！")
        finally:
            cursor.close()
            cnn.close()


# demo = Demo()
# demo.demo1()
# demo.demo2()
# demo.demo3()
# demo.demo4()
# demo.demo5()


class DataBase(object):
    def __init__(self):
        pass

    @staticmethod
    def get_database(database):
        config = {
            'host': 'localhost',
            'user': 'root',
            'password': '2020',
            'port': 3306,
            'database': database,
            'charset': 'utf8'
        }
        try:
            cnn = con.connect(**config)
        except con.Error as e:
            print("数据库链接失败", str(e))
        else:
            return cnn

    @staticmethod
    def create_table(database, table):
        cursor = database.cursor(buffered=True)
        try:
            cursor.execute(table)
        except con.Error as e:
            print("create table failed.", str(e))
        else:
            print("create table done.")
        finally:
            cursor.close()

    @staticmethod
    def del_table(database, name):
        cursor = database.cursor(buffered=True)
        del_sql = "drop table " + name
        try:
            cursor.execute(del_sql)
        except con.Error as e:
            print("table delete failed.", str(e))
        else:
            print("table delete done.", name)
        finally:
            cursor.close()

    @staticmethod
    def insert_data_one(database, cmd, data):
        cursor = database.cursor(buffered=True)
        try:
            cursor.execute(cmd, data)
        except con.Error as e:
            print("one data insert failed.", str(e))
        finally:
            cursor.close()

    @staticmethod
    def insert_data_many(database, cmd, datas):
        cursor = database.cursor(buffered=True)
        try:
            cursor.executemany(cmd, datas)
        except con.Error as e:
            print("datas insert failed.", str(e))
        finally:
            cursor.close()

    @staticmethod
    def query_data(database, table_name):
        cursor = database.cursor(buffered=True)
        query_sql = "select * from " + table_name
        try:
            cursor.execute(query_sql)
            res = cursor.fetchall()
        except con.Error as e:
            print("data query failed.", str(e))
        else:
            print("data query done.")
            cursor.close()
            return res
        finally:
            cursor.close()
            return []


class PyechartBar3dData(object):
    def __init__(self):
        # TODO：应该改为私有数据。
        self.data_base = DataBase.get_database("test")
        self.data_set = tool.FileFolderHandler.get_current_path()
        self.hours_name = "pyechart_demo\\bar3d_hours.txt"
        self.days_name = "pyechart_demo\\bar3d_days.txt"
        self.data_name = "pyechart_demo\\bar3d_data.txt"

    def __del__(self):
        self.data_base.close()

    def _read_file_data(self, file_name, delim=','):
        # TODO：这里有问题，当文件中没有换行符时。
        data = []
        path = tool.FileFolderHandler.join_dir_file(self.data_set, file_name)
        for line in tool.FileToData.read_data_yield(path, 1024):
            arr = tool.FileToData.split_data_line(line, delim)
            if len(arr) == 7 or len(arr) == 11:
                data = arr
            if len(arr) == 2:
                data.append(arr[0] + ',' + arr[1] + ',' + arr[2])
        return data

    def _create_table(self):
        table = "create table if not exists `py_bar3d_2`" \
                "(`id` char(20) not null," \
                "`hours` longtext not null," \
                "`days` longtext not null," \
                "`data` longtext  not null)" \
                "engine=MyISAM default charset=utf8 "

        cursor = self.data_base.cursor(buffered=True)
        try:
            cursor.execute(table)
        except con.Error as e:
            print("创建表失败", str(e))
        else:
            print("创建表成功！", "py_bar3d_2")

    def get_database_table(self):
        return "test", "py_bar3d_2"

    def _turn_data_to_str(self, data, delim=','):
        # no use now.
        data_str = ""
        for it in data:
            data_str = data_str + delim + it
        return data_str

    def _reset_data(self, data):
        # no use now.

        new_data = []
        for it in data:
            arr = it[1:-1]
            arr = arr.split(',')
            new_data.append('[' + arr[1] + "," + arr[0] + "," + arr[2] + ']')
        return new_data

    def _input_data(self):
        # 失败：希望将文件中数组读取出来连接为字符串存入表中，
        # 报错：“Data too long for column 'days' at row 1”
        self._create_table()

        hours = self._read_file_data(self.hours_name)
        days = self._read_file_data(self.days_name)
        hours_str = self._turn_data_to_str(hours)
        days_str = self._turn_data_to_str(days)

        data = self._read_file_data(self.data_name)
        data = self._reset_data(data)
        data_str = self._turn_data_to_str(data)

        cursor = self.data_base.cursor(buffered=True)
        table_insert = "insert into py_bar3d_demo(id, hours, days, data) values (%s, %s, %s, %s)"
        table_data = ("bar3d", hours_str, days_str, data_str)
        try:
            cursor.execute(table_insert, table_data)
        except con.Error as e:
            print("表插入失败", str(e))
        else:
            print("表插入成功")
        finally:
            cursor.close()

    def input_data_file(self):
        # 出现“data too long for column”时， set @@global sql_mode='';

        self._create_table()
        cursor = self.data_base.cursor(buffered=True)

        hours_path = tool.FileFolderHandler.join_dir_file(self.data_set, self.hours_name)
        days_path = tool.FileFolderHandler.join_dir_file(self.data_set, self.days_name)
        data_path = tool.FileFolderHandler.join_dir_file(self.data_set, self.data_name)

        table_insert = "insert into py_bar3d_2(id, hours, days, data) values (%s, %s, %s, %s)"
        table_data = ("bar3d", hours_path, days_path, data_path)
        try:
            cursor.execute(table_insert, table_data)
        except con.Error as e:
            print("插入表失败", str(e))
        else:
            print("插入表成功！")
            cursor.close()

    def out_data(self):
        # 返回表的所有行数据（列表）；每张表的数据（元组）
        cursor = self.data_base.cursor(buffered=True)
        table_query = "select * from py_bar3d_2"
        try:
            cursor.execute(table_query)
            res = cursor.fetchall()
        except con.Error as e:
            print("查询数据失败")
        else:
            print("查询数据成功！")
            cursor.close()
            return res

    def del_data(self, Id):
        cursor = self.data_base.cursor(buffered=True)
        table_del = "delete from py_bar3d_2 where id = %s"
        try:
            cursor.execute(table_del, (Id,))
        except con.Error as e:
            print("表数据删除失败", str(e))
        else:
            print("表数据删除成功！")
            cursor.close()

    def _del_table(self):
        cursor = self.data_base.cursor(buffered=True)
        sql_del_1 = "DROP TABLE py_bar3d_demo"
        sql_del_2 = "drop table py_bar3d"
        try:
            cursor.execute(sql_del_1)
            cursor.execute(sql_del_2)
        except con.Error as e:
            print("表删除失败", str(e))
        else:
            print("表删除成功！")
            cursor.close()


# demo = PyechartBar3dData()
# demo.del_data("bar3d")
# demo.input_data_file()
# demo.del_table()
# res = demo.out_data()
# print(res)


class PyechartGraphData(object):
    def __init__(self):
        self._url = "https://echarts.baidu.com/examples/data/asset/data/npmdepgraph.min10.json"
        self._sql = DataBase.get_database("test")
        self._data_file = "E:\\Folder\\hydroprotools\\data_set\\pyechart_demo\\graph.txt"
        self._tables = []

    def __del__(self):
        self._sql.close()

    def create_table(self):
        # table = "create table if not exists `graph_dict`" \
        #           "(`id` varchar(25) not null," \
        #           "`nodes` varchar(25) not null," \
        #           "`edges` varchar(25) not null," \
        #           "primary key(`id`))" \
        #           "engine=MyISAM " \
        #           "default charset=utf8"
        table_node = "create table if not exists `nodes_dict`" \
                     "(`id` varchar(255) not null," \
                     "`x` float(12, 7) not null," \
                     "`y` float(12, 7) not null," \
                     "`name` varchar(255) not null," \
                     "`symbolSize` float(15, 12) not null," \
                     "`itemStyle` varchar(25) not null)" \
                     "engine=MyISAM " \
                     "default charset=utf8"
        table_edge = "create table if not exists `edges_dict`" \
                     "(`source` varchar(25) not null," \
                     "`target` varchar(25) not null)" \
                     "engine=MyISAM " \
                     "default charset=utf8"

        cursor = self._sql.cursor(buffered=True)
        try:
            # cursor.execute(table)
            # DataBase.del_table(self._sql, "graph_dict")
            DataBase.del_table(self._sql, "nodes_dict")
            cursor.execute(table_node)
            DataBase.del_table(self._sql, "edges_dict")
            cursor.execute(table_edge)
        except con.Error as e:
            print("创建表失败", str(e))
        else:
            print("创建表成功！")
            cursor.close()
            self._tables = ["nodes_dict", "edges_dict"]

    def get_table_names(self):
        return self._tables

    def get_local_data_file(self):
        return self._data_file

    def _extract_data(self):
        async def get_json_data(url: str) -> dict:
            async with ClientSession(connector=TCPConnector(ssl=False)) as session:
                async with session.get(url=url) as respone:
                    return await respone.json()
        data = asyncio.run(get_json_data(self._url))
        return data

    def input_data_to_sql(self):
        data = self._extract_data()

        insert_node = "insert into nodes_dict(id, x, y, name, symbolSize, itemStyle) " \
                      "values (%s, %s, %s, %s, %s, %s)"
        data_nodes = []
        for node in data["nodes"]:
            data_node = [node["id"], node["x"], node["y"], node["label"], node["size"], node["color"]]
            DataBase.insert_data_one(self._sql, insert_node, data_node)
        # DataBase.insert_data_many(self._sql, insert_node, data_nodes)

        insert_edge = "insert into edges_dict(source, target) values (%s, %s)"
        # data_edges = []
        for edge in data["edges"]:
            # data_edges.append([edge["sourceID"], edge["targetID"]])
            data_edge = [edge["sourceID"], edge["targetID"]]
            DataBase.insert_data_one(self._sql, insert_edge, data_edge)
        # DataBase.insert_data_many(self._sql, insert_edge, data_edges)

        print("表数据插入成功！")

    def query_nodes_data(self):
        nodes = DataBase.query_data(self._sql, "nodes_dict")
        print(len(nodes))
        return nodes

    def query_edges_data(self):
        edges = DataBase.query_data(self._sql, "edges_dict")
        print(len(edges))
        return edges


# graph = PyechartGraphData()
# graph.create_table()
# graph.input_data_to_sql()
# graph.query_edges_data()
# graph.query_nodes_data()










