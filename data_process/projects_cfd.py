from math import e
from math import pi
import os

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib as mpl
import numpy as np

from pyecharts.charts import Surface3D, Scatter3D
from pyecharts.faker import Faker
from pyecharts import options as opts

from toolkits import filetools as ft
from data_visualize import data_visualize as visu


class ReservoirProblem(object):
    # <computational hydraulics>. chapter4.6.1

    def __init__(self):
        self.q0 = 50
        self.deltaT = 40
        self.k = 0.01
        self.totalT = 401

    def analytical_solution(self):
        res = []
        for t in range(0, self.totalT, self.deltaT):
            res.append(self.q0*pow(e, -t*self.k))
        return res

    def implicit_solution(self):
        res = [self.q0]
        last_q = self.q0
        for t in range(0, self.totalT, self.deltaT):
            cur_q = (1 - self.k*self.deltaT)*last_q
            last_q = cur_q
            res.append(cur_q)
        return res

    def explicit_solution(self):
        res = [self.q0]
        last_q = self.q0
        for t in range(0, self.totalT, self.deltaT):
            cur_q = last_q/(1 + self.k*self.deltaT)
            last_q = cur_q
            res.append(cur_q)
        return res

    def res_visualize(self):
        time = [t for t in range(0, self.totalT, self.deltaT)]
        ana_res = self.analytical_solution()
        exp_res = self.explicit_solution()
        imp_res = self.implicit_solution()

        plt.plot(time, ana_res, label="ana_res")
        plt.plot(time, exp_res[:-1], label="exp_res")
        plt.plot(time, imp_res[:-1], label="imp_res")
        plt.legend()
        plt.show()


# sul = ReservoirProblem()
# sul.res_visualize()


class FloodRiverProblem(object):
    # <computational hydraulics>. chapter4.6.2

    def __init__(self):
        self.totalT = 6e4
        self.totalL = 6e4
        self.deltaT = 1.5e3
        self.deltaL = 3e3
        self.c = 1
        self.coeff = self.c*self.deltaT/self.deltaL
        self.last_res = self.init_condition()

    def left_boundary_condition(self, t):
        return 2.0

    def init_condition(self):
        ini = [2.0 for i in np.arange(0, self.totalL, self.deltaL)]
        ini[2] = 4.0
        ini[3] = 6.0
        ini[4] = 4.0
        return ini

    def fdm_compute(self):
        res = []
        for i in np.arange(0, self.totalL, self.deltaL):
            n = int(i / self.deltaL)
            value = self.last_res[n]
            res.append([0, i, value])

        for t in np.arange(self.deltaT, self.totalT, self.deltaT):
            res.append([t, 0, self.left_boundary_condition(t)])
            for i in np.arange(self.deltaL, self.totalL, self.deltaL):
                n = int(i/self.deltaL)
                value = (1-self.coeff)*self.last_res[n] + self.coeff*self.last_res[n-1]
                res.append([t, i, value])
            self.last_res = [res[i][2] for i in range(len(res)-len(self.last_res), len(res), 1)]
        return res

    def res_visualize(self) -> Scatter3D:
        chart = (Scatter3D()
                 .add("", self.fdm_compute())
                 .set_global_opts(title_opts=opts.TitleOpts("flood_river_cfl"))
                 )
        return chart


# sol = FloodRiverProblem()
# sol.res_visualize().render()

# res = sol.fdm_compute()
# writer = ft.DataToFile()
# writer.input_list(res, len(res[0]))
# writer.write_file("C:\\Users\\gr\\Desktop")


class DiffusionProblem(object):
    # <computational hydraulics>. chapter4.6.3

    def __init__(self):
        self.totalT = 600.0
        self.totalL = 600.0
        self.deltaT = 30.0
        self.deltaL = 50.0
        self.dm = 0.01*3600.0
        self.m = 0.7
        self.r = self.dm*self.deltaT/(self.deltaL*self.deltaL)
        self.last_value = self.init_condition()

    def init_condition(self):
        init_value = [0.0 for i in np.arange(0, self.totalL+1.0, self.deltaL)]
        init_value[int(250/self.deltaL)] = 10
        init_value[int(300/self.deltaL)] = 10
        return init_value

    def ana_res(self):
        ana_res = []
        for i in np.arange(0, self.totalL, self.deltaL):
            ind = int(i/self.deltaL)
            value = self.last_value[ind]
            ana_res.append([0.0, i, value])

        for t in np.arange(self.deltaT, self.totalT, self.deltaT):
            for i in np.arange(0, self.totalL, self.deltaL):
                power = -pow(i - 275, 2) / (4 * self.dm * t)
                value = pow(e, power) * self.m / (2 * pow(pi * self.dm * t, 0.5))
                ana_res.append([t, i, value*1e3])
        return ana_res

    def ftbs_res(self):
        # failed.

        ftbs_res = []
        for i in np.arange(0, self.totalL, self.deltaL):
            ind = int(i/self.deltaL)
            ftbs_res.append([0.0, i, self.last_value[ind]])

        for t in np.arange(self.deltaT, self.totalT, self.deltaT):
            cur_value = self.last_value
            ftbs_res.append([t, 0, 0])
            for i in np.arange(250, 0, -self.deltaL):
                ind = int(i/self.deltaL)
                value = self.last_value[ind]
                if ind >= 2:
                    value = (1-self.r)*self.last_value[ind] \
                            + 2*self.r*self.last_value[ind-1] \
                            - self.r*self.last_value[ind-2]
                elif ind >= 1:
                    value = (1-self.r)*self.last_value[ind] \
                            + 2*self.r*self.last_value[ind-1]
                ftbs_res.append([t, i, value])
                cur_value[ind] = value

            ftbs_res.append([t, self.totalL, 0])
            for i in np.arange(300, self.totalL+1.0, self.deltaL):
                ind = int(i / self.deltaL)
                value = self.last_value[ind]
                if ind <= (self.totalL/self.deltaL - 2):
                    value = (1 - self.r) * self.last_value[ind] \
                            + 2 * self.r * self.last_value[ind + 1] \
                            - self.r * self.last_value[ind + 2]
                elif ind <= (self.totalL/self.deltaL - 1):
                    value = (1 - self.r) * self.last_value[ind] \
                            + 2 * self.r * self.last_value[ind + 1]
                ftbs_res.append([t, i, value])
                cur_value[ind] = value
            self.last_value = cur_value
        return ftbs_res

    def ftfs_res(self):
        # sucessed.

        ftcs_res = []
        for i in np.arange(0, self.totalL, self.deltaL):
            ind = int(i/self.deltaL)
            ftcs_res.append([0.0, i, self.last_value[ind]])

        cur_value = []
        for t in np.arange(self.deltaT, self.totalT, self.deltaT):
            ftcs_res.append([t, 0, 0])
            cur_value.append(0)
            for i in np.arange(self.deltaL, self.totalL, self.deltaL):
                ind = int(i/self.deltaL)
                value = self.r*self.last_value[ind+1] \
                        + (1-2*self.r)*self.last_value[ind] \
                        + self.r*self.last_value[ind-1]
                cur_value.append(value)
                ftcs_res.append([t, i, value])
            cur_value.append(0)
            ftcs_res.append([t, self.totalL, 0])

            for it in range(0, len(cur_value), 1):
                self.last_value[it] = cur_value[it]
            cur_value.clear()
        return ftcs_res

    def res_visualize(self) -> Scatter3D:
        chart = (
            Scatter3D(init_opts=opts.InitOpts(width="1000px", height="800px"))
            .add("ana_res", self.ana_res())
            # .add("ftbs_res", self.ftbs_res())
            .add("ftcs_res", self.ftfs_res())
            .set_global_opts(title_opts=opts.TitleOpts("diffusion_cfd"))
        )
        return chart


# sol = DiffusionProblem()
# sol.res_visualize().render("demo.html")


class SteadyDiffusionProblem(object):
    # <应用有限体积法求解对流扩散问题>, chapter2.3.1
    # 关键是积分方程的离散近似
    # failed.

    def __init__(self):
        self.totalL = 1.0
        self.discreteNum = 1
        self.deltaL = 1.0
        self.x_arr = [0.0, 1.0]

    def domain_discrete(self, Num):
        self.discreteNum = Num
        self.deltaL = self.totalL/Num
        self.x_arr = [0.0+i*self.deltaL for i in range(0, Num+1, 1)]

    def ana_res(self, Num):
        res = []
        self.domain_discrete(Num)

        for x in self.x_arr:
            value = (pow(e, x) - pow(e, -x))*e/(e*e - 1)
            res.append(value)
        return res

    def _create_coeff_matrix(self):
        coeff = []

        # coeff matrix A
        mat1 = []
        const = self.deltaL
        mat1.append([0.0 for i in range(0, self.discreteNum+1, 1)])
        for k in range(1, self.discreteNum+1, 1):
            row = []
            for i in range(0, self.discreteNum+1, 1):
                if i == 0 or i == k:
                    row.append(0.5*const)
                elif 0 < i < k:
                    row.append(const)
                else:
                    row.append(0.0)
            mat1.append(row)

        # coeff matrix I-A·A
        mat1 = np.matmul(mat1, mat1)
        mat2 = []
        for k in range(0, self.discreteNum+1, 1):
            mat2.append([0.0 for i in range(0, self.discreteNum+1, 1)])

        for k in range(0, self.discreteNum+1, 1):
            for i in range(0, self.discreteNum+1, 1):
                if i == k:
                    mat2[k][i] = 1.0 - mat1[k][i]
                else:
                    mat2[k][i] = - mat1[k][i]

        # add vars C0,C1
        for k in range(0, self.discreteNum+1, 1):
            mat2[k].append(-self.x_arr[k])
            mat2[k].append(-1.0)

        # add boundary conditions
        con1, con2 = [1.0], []
        for i in range(1, self.discreteNum+3, 1):
            con1.append(0.0)
        for i in range(0, self.discreteNum+1, 1):
            if i == 0 or i == self.discreteNum:
                con2.append(0.5*self.deltaL)
            else:
                con2.append(self.deltaL)
        con2.append(1.0)
        con2.append(0.0)

        # consist of total coeff matrix
        coeff.extend(mat2)
        coeff.append(con1)
        coeff.append(con2)
        return coeff

    def _create_const_matix(self):
        const = [0.0 for i in range(0, self.discreteNum+2, 1)]
        const.append(1.0)
        return const

    def fvm_res(self, Num):
        self.domain_discrete(Num)
        # 求解线性方程组的思路，将所有方程都组织成A·X = b
        # 其中，所有未知量都放在X，系数矩阵A和右端项b均已知

        coeff = self._create_coeff_matrix()
        const = self._create_const_matix()
        res = np.linalg.solve(coeff, const)
        return res[:-2]

    def data_visualize(self):
        ana_res = self.ana_res(5)
        plt.plot(self.x_arr, ana_res, label="ana_res")
        fvm_res_5 = self.fvm_res(5)
        plt.plot(self.x_arr, fvm_res_5, label="fvm_res_5")
        fvm_res_50 = self.fvm_res(50)
        plt.plot(self.x_arr, fvm_res_50, label="fvm_res_50")
        plt.legend()
        plt.show()

    def test(self):
        self.domain_discrete(5)

        mat = []
        const = self.deltaL*self.deltaL
        for k in range(0, self.discreteNum+1, 1):
            row = []
            for i in range(0, self.discreteNum+1, 1):
                if i == 0:
                    row.append(const*(0.5*k - 0.25))
                elif 0 < i < k:
                    row.append(const*(k - i))
                elif i == k:
                    row.append(const*0.25)
                else:
                    row.append(0.0)
            mat.append(row)

        mat2 = []
        const = self.deltaL
        for k in range(0, self.discreteNum+1, 1):
            row = []
            for i in range(0, self.discreteNum+1, 1):
                if i == 0 or i == k:
                    row.append(0.5*const)
                elif 0 < i < k:
                    row.append(const)
                else:
                    row.append(0.0)
            mat2.append(row)
        # mat2 = np.matmul(mat2, mat2)
        mat2 = np.dot(mat2, mat2)


# sol = SteadyDiffusionProblem()
# sol.data_visualize()
# sol.test()


class SteadyAdvectionProblem(object):
    # <应用有限体积法求解对流扩散问题>, chapter2.3.2

    def __init__(self):
        self.totalL = 1.0
        self.Num = 5
        self.pe = 5
        self.alpha = 0.525
        self.deltaL = self.totalL/self.Num
        self.xarr = [0.0, 1.0]

    def discrete_domain(self, Num):
        self.Num = Num
        self.deltaL = self.totalL/Num
        self.xarr = [i*self.deltaL for i in range(0, Num+1, 1)]

    def ana_res(self, Num):
        self.discrete_domain(Num)

        res = []
        temp = pow(e, self.pe) - 1.0
        for x in self.xarr:
            value = 1.0 - (pow(e, self.pe*x) - 1.0)/temp
            res.append(value)
        return res

    def _get_coeff_matrix(self):
        coeff = []

        # A
        mat = [[0.0 for i in range(0, self.Num+1, 1)]]
        for k in range(1, self.Num+1, 1):
            row = []
            for i in range(0, self.Num+1, 1):
                if i == 0:
                    row.append(self.deltaL*self.alpha)
                elif 0 < i < k:
                    row.append(self.deltaL)
                elif i == k:
                    row.append(self.deltaL*(1.0 - self.alpha))
                else:
                    row.append(0.0)
            mat.append(row)

        # I-Pe·A-i-x
        coeff.append([0.0 for i in range(0, self.Num+1, 1)])
        coeff[0][0] = 1.0
        coeff[0].append(-1.0)
        coeff[0].append(0.0)

        for k in range(1, self.Num+1, 1):
            row = []
            for i in range(0, self.Num+1, 1):
                if i == k:
                    row.append(1.0 - self.pe*mat[k][i])
                else:
                    row.append(- self.pe*mat[k][i])
            row.append(-1.0)
            row.append(-self.xarr[k])
            coeff.append(row)

        # add boundary condition
        row = [0.0 for i in range(0, self.Num+3, 1)]
        row[0] = 1.0
        coeff.append(row)
        row = [0.0 for i in range(0, self.Num+3, 1)]
        row[-3] = 1.0
        coeff.append(row)

        return coeff

    def _get_const(self):
        const = [0.0 for i in range(0, self.Num+3, 1)]
        const[-2] = 1.0
        return const

    def fvm_res(self, Num):
        self.discrete_domain(Num)

        coeff = self._get_coeff_matrix()
        const = self._get_const()
        res = np.linalg.solve(coeff, const)
        return res[:-2]

    def visualize_data(self):
        # res = self.ana_res(5)
        # plt.plot(self.xarr, res, label="ana_res")
        res_50 = self.ana_res(50)
        plt.plot(self.xarr, res_50, label="ana_res_50")
        # fvm_res = self.fvm_res(5)
        # plt.plot(self.xarr, fvm_res, label="fvm_res")
        fvm_res_50 = self.fvm_res(50)
        plt.plot(self.xarr, fvm_res_50, label="fvm_res_50")
        plt.legend()
        plt.show()


# sol = SteadyAdvectionProblem()
# sol.visualize_data()


class SteadyAdvDifProblem(object):
    # <应用有限体积法求解对流扩散问题>, chapter2.3.3
    # failed

    def __init__(self):
        self.totalL = 1.0
        self.Num = 5
        self.pe = 100
        self.fi = 2.0
        self.alpha = 0.775
        self.deltaL = 1.0
        self.xarr = [0.0, 1.0]

    def discrete_domain(self, Num):
        self.Num = Num
        self.deltaL = self.totalL/Num
        self.xarr = [i*self.deltaL for i in range(0, Num+1, 1)]

    def ana_res(self, Num):
        self.discrete_domain(Num)
        res = []

        m1 = 0.5*(self.pe + pow(self.pe*self.pe + 4*self.fi*self.fi, 0.5))
        m2 = 0.5*(self.pe - pow(self.pe*self.pe + 4*self.fi*self.fi, 0.5))

        temp1 = pow(e, m1) - 1.0
        temp2 = 1.0 - pow(e, m2)
        temp3 = pow(e, m1) - pow(e, m2)
        for x in self.xarr:
            value = (pow(e, m2*x)*temp1 + pow(e, m1*x)*temp2)/temp3
            res.append(value)
        return res

    def _get_coeff_matrix(self):
        coeff = []

        # pe*A
        mat = [[0.0 for i in range(0, self.Num+1, 1)]]
        for k in range(1, self.Num+1, 1):
            row = []
            for i in range(0, self.Num+1, 1):
                if i == 0:
                    row.append(self.pe*self.alpha*self.deltaL)
                elif 0 < i < k:
                    row.append(self.pe*self.deltaL)
                elif i == k:
                    row.append(self.pe*(1.0-self.alpha)*self.deltaL)
                else:
                    row.append(0.0)
            mat.append(row)

        # fi^2*A2
        fi2 = self.fi*self.fi
        deltaL2 = self.deltaL*self.deltaL
        mat2 = [[0.0 for i in range(0, self.Num+1, 1)]]
        for k in range(1, self.Num+1, 1):
            row = []
            for i in range(0, self.Num+1, 1):
                if i == 0:
                    row.append(fi2*(k-self.alpha)*self.alpha*deltaL2)
                elif 0 < i < k:
                    row.append(fi2*(k-i+1.0-2.0*self.alpha)*deltaL2)
                elif i == k:
                    row.append(fi2*pow(1.0-self.alpha, 2.0)*deltaL2)
                else:
                    row.append(0.0)
            mat2.append(row)

        # I - pe*A - fi^2*A2 - x - i
        for k in range(0, self.Num+1, 1):
            row = []
            for i in range(0, self.Num+1, 1):
                if i == k:
                    row.append(1.0 - mat[k][i] - mat2[k][i])
                else:
                    row.append(- mat[k][i] - mat2[k][i])
            row.append(-self.xarr[k])
            row.append(-1.0)
            coeff.append(row)

        # add boundary condition
        con1 = [0.0 for i in range(0, self.Num+3, 1)]
        con1[0] = 1.0
        con2 = [0.0 for i in range(0, self.Num+3, 1)]
        con2[1] = 1.0
        coeff.append(con1)
        coeff.append(con2)
        return coeff

    def _get_const_matrix(self):
        const = [0.0 for i in range(0, self.Num+3, 1)]
        const[-2] = 1.0
        const[-1] = 1.0
        return const

    def fvm_res(self, Num):
        self.discrete_domain(Num)

        coeff = self._get_coeff_matrix()
        const = self._get_const_matrix()
        res = np.linalg.solve(coeff, const)
        return res[:-2]

    def visualize_data(self):
        res = self.ana_res(10)
        plt.plot(self.xarr, res, label="ana_res")
        fvm_res_10 = self.fvm_res(10)
        plt.plot(self.xarr, fvm_res_10, label="fvm_res_10")
        fvm_res_100 = self.fvm_res(100)
        plt.plot(self.xarr, fvm_res_100, label="fvm_res_100")
        plt.legend()
        plt.show()


# sol = SteadyAdvDifProblem()
# sol.visualize_data()


class SteadyAdvDifProblem2D(object):
    def __init__(self):
        self.totalX = 1.0
        self.totalY = 1.0
        self.Num = 1
        self.deltaX = 1.0
        self.deltaY = 1.0
        self.xarr = [0.0, 1.0]
        self.yarr = [0.0, 1.0]

    def discrete_domain(self, Num):
        if Num <= 0:
            raise ValueError("Invalid num", Num)
        self.Num = Num
        self.deltaX = self.totalX/Num
        self.deltaY = self.totalY/Num
        self.xarr = [i*self.deltaX for i in range(0, Num+1, 1)]
        self.yarr = [i*self.deltaX for i in range(0, Num+1, 1)]

    def ana_res(self, Num):
        self.discrete_domain(Num)
        for y in self.yarr:
            for x in self.xarr:
                z = x*y*(1.0 - x)*(1.0 - y)
                yield [x, y, z]

    def visualize_res(self) -> Surface3D:
        chart = (
            Surface3D(init_opts=opts.InitOpts(width="1000px", height="800px"))
            .add(
                series_name="ana_res",
                shading="color",
                data=list(self.ana_res(20)),
                xaxis3d_opts=opts.Axis3DOpts(type_="value"),
                yaxis3d_opts=opts.Axis3DOpts(type_="value"),
                grid3d_opts=opts.Grid3DOpts(width=100, height=80, depth=100)
            )
            .set_global_opts(visualmap_opts=opts.VisualMapOpts(
                dimension=2, max_=1.0, min_=0
            ))
        )
        return chart


# sol = SteadyAdvDifProblem2D()
# sol.visualize_res().render("surface3d.html")


def generate_IN1D_demo():
    deltaL = 200
    slopL = 20
    Length = {"HD1": [2000, 0.0001, 100, 0.3, 0.1],
              "HD10": [2000, 0.0001, 100, 0.2, 0.0],
              "HD2": [1000, 0.0002, 50, 0.5, 0.3],
              "HD4": [1000, 0.0002, 50, 0.5, 0.3],
              "HD5": [1000, 0.0002, 50, 0.7, 0.5],
              "HD7": [1000, 0.0002, 50, 0.5, 0.3],
              "HD8": [1000, 0.0002, 50, 0.5, 0.3],
              "HD3": [1000, 0.0001, 75, 0.1, 0.0],
              "HD6": [1000, 0.0001, 75, 0.3, 0.2],
              "HD9": [1000, 0.0001, 75, 0.2, 0.1]}

    path = "C:\\Users\\gr\\Desktop\\res.txt"
    file = open(path, "w")

    hd_num = 1
    for keys, values in Length.items():
        file.write("***************************************" + keys + "\n")
        dm_count = int(values[0] / deltaL) + 1
        file.write(str(dm_count) + "\n")

        node_num = 4
        for i in range(1, dm_count + 1, 1):
            file.write(str(i) + "\t0\t" + str(node_num) + "\t" + str(hd_num) + "\n")
            ele = round(values[3] - (i - 1) * deltaL * values[1], 3)
            file.write("\t0\t10\n")
            file.write("\t" + str(slopL) + "\t" + str(ele) + "\n")
            file.write("\t" + str(slopL + values[2]) + "\t" + str(ele) + "\n")
            file.write("\t" + str(2 * slopL + values[2]) + "\t10\n")
            hd_num = hd_num + 1
    file.close()

# demo = generate_IN1D_demo()


class RiverSystemModel1D(object):
    # <computational hydraulics>, chapter7.2.1

    def __init__(self,
                 L, T, deltaL, deltaT, sita, fi,
                 upstream, downstream, n, s0):
        self._totalL = L
        self._totalT = T
        self._deltaL = deltaL   # 不考虑计算域无法整除的情况。
        self._deltaT = deltaT
        self._sita = sita
        self._fi = fi
        self._upstream_bc = upstream
        self._downstream_bc = downstream
        self._n = n     # 糙率
        self._s0 = s0   # 底坡
        self._B = 100   # 断面宽度
        self._H = 5     # 断面深度
        self._g = 9.81
        self._xarr = []
        self._aarr = []
        self._qarr = []
        self._harr = []

    def __del__(self):
        pass

    def discrete_domain(self):
        num = int(self._totalL/self._deltaL) + 1
        self._xarr = [self._deltaL*i for i in range(0, num, 1)]

    def init_condition(self):
        num = int(self._totalL/self._deltaL) + 1
        self._qarr = [0.0 for i in range(num)]
        self._harr = [0.0 for i in range(num)]
        self._aarr = [self._B*self._H for i in range(num)]

    def _iterpolate(self, arr, t):
        ind = -1
        for flow in arr:
            if flow[0] <= t:
                ind = arr.index(flow)
        if ind == -1 or ind == len(arr):
            return arr[-1][1]
        else:
            last_bc = arr[ind]
            next_bc = arr[ind+1]
            q = last_bc[1] + (t - last_bc[0])*(next_bc[1]-last_bc[1])/(next_bc[0]-last_bc[0])
            return q

    def upstream_boundary_condition(self, t):
        return self._iterpolate(self._upstream_bc, t)

    def downstream_boundary_condition(self, t):
        return self._iterpolate(self._downstream_bc, t)

    def compute_equation_coeff(self, i):
        # 计算下个点方程系数。

        # 连续方程
        a1 = -self._sita/self._deltaL
        b1 = (1.0 - self._fi)*self._B/self._deltaT
        c1 = -a1
        d1 = self._B*self._fi/self._deltaT

        e11 = (1.0-self._sita)*(self._qarr[i] - self._qarr[i+1])/self._deltaL
        e12 = self._B*((1.0-self._fi)*self._harr[i] + self._fi*self._harr[i+1])/self._deltaT
        e1 = e11 + e12

        # 动量方程
        a21 = (1.0 - self._fi)/self._deltaT
        A1 = 0.5*(self._aarr[i] + self._aarr[i])
        a23 = - self._qarr[i]/(self._deltaL*A1)
        h = self._aarr[i]/self._B
        r = self._aarr[i]/(2*h + self._B)
        k1 = self._aarr[i]*pow(r, 2.0/3.0)/self._n
        a24 = (1.0 - self._fi)*A1*abs(self._qarr[i])/k1**2
        a2 = a21 + a23 + a24
        A2 = 0.5*(self._aarr[i+1] + self._aarr[i+1])
        A = 0.5*(A1 + A2)
        b2 = -self._g*A2*self._sita/self._deltaL
        c21 = self._fi/self._deltaT
        c22 = self._qarr[i+1]/(self._deltaL*A2)
        h = self._aarr[i+1]/self._B
        r = self._aarr[i+1]/(2*h + self._B)
        k2 = self._aarr[i]*pow(r, 2.0/3.0)/self._n
        c23 = self._g*self._fi*A2*abs(self._qarr[i+1])/k2**2
        c2 = c21 + c22 + c23
        d2 = self._g*self._fi*A2*self._sita/self._deltaL
        e21 = (1-self._fi)*self._qarr[i]/self._deltaT
        e22 = self._fi*self._qarr[i]/self._deltaT
        e23 = self._g*A*(1-self._sita)*(self._harr[i] - self._harr[i])/self._deltaL
        e24 = self._g*A*self._s0
        e2 = e21 + e22 + e23 + e24
        return a1, b1, c1, d1, e1, a2, b2, c2, d2, e2

    def compute_sweep_coeff(self, equ_coeff, f, g):
        h = -equ_coeff[2]/(equ_coeff[0]*f + equ_coeff[1])
        i = -equ_coeff[3]/(equ_coeff[0]*f + equ_coeff[1])
        j = (equ_coeff[4] - equ_coeff[0]*g)/(equ_coeff[0]*f + equ_coeff[1])

        f1 = equ_coeff[-2]*(equ_coeff[0]*f + equ_coeff[1])
        f2 = equ_coeff[3]*(equ_coeff[5]*f + equ_coeff[6])
        f3 = equ_coeff[2]*(equ_coeff[5]*f + equ_coeff[6])
        f4 = equ_coeff[2]*(equ_coeff[0]*f + equ_coeff[1])
        nextf = (f1 + f2)/(f3 + f4)

        g1 = equ_coeff[-1] - equ_coeff[5]*f*j - equ_coeff[5]*g - equ_coeff[6]*j
        g2 = h*(equ_coeff[5]*f + equ_coeff[6]) + equ_coeff[-3]
        nextg = g1/g2
        return h, i, j, nextf, nextg

    def generate_matrix(self):
        pass

    def excute_forward_sweep(self):
        pass

    def excute_backward_sweep(self):
        pass

    def run_preissmann_model(self):
        pass

    def visualize_data(self):
        pass


upstream_flow = [
[0.02 ,4.19   ],
[11.33 ,24.19 ],
[15.81 ,56.45 ],
[19.29 ,137.10],
[21.41 ,225.81],
[23.54 ,325.27],
[26.01 ,427.42],
[28.79 ,486.56],
[30.17 ,500.00],
[32.22 ,489.25],
[34.92 ,424.73],
[37.60 ,330.65],
[40.28 ,236.56],
[43.99 ,145.16],
[48.73 ,69.89 ],
[52.83 ,40.32 ],
[60.02 ,26.88 ],
[75.44 ,21.51 ],
[94.30 ,21.51 ],
[109.04,21.51 ],
[120.02,24.19 ],
]

downstream_depth = [
[0.00 ,0.22   ],
[11.31 ,0.22  ],
[14.74 ,0.37  ],
[16.46 ,0.66  ],
[19.20 ,1.37  ],
[21.60 ,2.20  ],
[24.00 ,3.18  ],
[26.40 ,3.91  ],
[28.80 ,4.43  ],
[31.54 ,4.57  ],
[33.60 ,4.45  ],
[36.69 ,3.94  ],
[40.11 ,3.20  ],
[43.89 ,2.27  ],
[47.66 ,1.44  ],
[52.80 ,0.73  ],
[57.26 ,0.44  ],
[64.46 ,0.27  ],
[77.83 ,0.24  ],
[92.23 ,0.22  ],
[104.57,0.22  ],
[120.00,0.22  ],
]


class PreissmannSlotMethod(object):
    def __init__(self):
        pass

    def __del__(self):
        pass










