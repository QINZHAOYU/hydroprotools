# coding=utf8


def calculate_pa(rain_by_day, k) -> float:
    if rain_by_day.size() < 1: return 0

    pa = rain_by_day[0]
    for rain in rain_by_day:
        pa = pow(0.5*pa + rain, k)
    return pa


def msjk_flood(K, X, N, dt, nt, inflow, initflow=None) -> list:
    x1 = 2*K*X
    x2 = 2*K - x1
    if x1 <= dt <= x2: return []

    x1 = K - K*X + 0.5*dt
    c0 = (0.5*dt - K*X)/x1
    c1 = (K*X + 0.5*dt)/x1
    c2 = (K - K*X - 0.5*dt)/x1

    if initflow is None:
        initflow = [inflow[0] for i in range(0, len(inflow))]

    outflow = [initflow[-1]]
    in_q1, in_q2 = .0, .0
    out_q1, out_q2 = .0, .0
    for ind in range(1, nt):
        for i in range(0, N):
            out_q1 = initflow[i]
            if i == 0:
                in_q1, in_q2 = inflow[ind - 1], inflow[ind]
            out_q2 = c0*in_q2 + c1*in_q1 + c2*in_q1
            in_q1 = out_q1
            in_q2 = out_q2
            initflow[i] = out_q2
        outflow[ind] = out_q2
    return outflow


def add_basic_flow_to(outflow, entrance_init_flow, area_coe, decay_coe):
    if entrance_init_flow <= 0: return
    entrance_init_flow = entrance_init_flow*area_coe
    for i in range(0, len(outflow)):
        outflow[i] = outflow[i] + entrance_init_flow*pow(entrance_init_flow, i)


def PPaR_runoff(rain_by_hour, w0, p0, r, init_loss, pa_decay_coe, dt, pa, model) -> list:
    if model == 1: # 计算累积降雨
        for i in range(1, len(rain_by_hour)):
            rain_by_hour[i] = rain_by_hour[i] + rain_by_hour[i-1]

    runoff = []
    for i in range(0, len(rain_by_hour)):
        if rain_by_hour[i] < 0.0:
            rain_by_hour[i] = 0.0
        if pa < w0[0]: pa = w0[0]   # 如果pa值小于曲线中最小的，则以最小的为准

        # 根据pa值匹配PR曲线
        r_ind = len(r)
        for j in range(0, len(r)):
            if abs(w0[j] - pa) < 0.001:
                r_ind = j
                break

        if r_ind < len(r):  # 匹配到r曲线
            if rain_by_hour[i] < p0[0]: pass
            elif rain_by_hour[i] > p0[-1]:
                temp1 = rain_by_hour[i] - p0[-1]
                temp2 = r[r_ind][-1] - r[r_ind][-2]
                temp3 = p0[-1] - p0[-2]
                runoff[i] = r[r_ind][-1] + temp1*temp2/temp3
            else:
                for k in range(0, len(r[0])-1):
                    if p0[k] <= rain_by_hour[i] <= p0[k+1]:
                        temp1 = rain_by_hour[i] - p0[k]
                        temp2 = r[r_ind][k+1] - r[r_ind][k]
                        temp3 = p0[k+1] - p0[k]
                        runoff[i] = r[r_ind][k] + temp1*temp2/temp3
                        break
        else:
            for j in range(0, len(r)-1):
                if w0[j] < pa < w0[j+1]:
                    r_ind = j
                    break
            if w0[-1] < pa <= init_loss: r_ind = len(r) -1
            R1, R2 = 0.0, 0.0
            if rain_by_hour[i] < p0[0]:
                if r_ind >= len(r) -1: R2 = rain_by_hour[i]
            elif rain_by_hour[i] > p0[-1]:
                temp1 = rain_by_hour[i] - p0[-1]
                temp2 = r[r_ind][-1] - r[r_ind][-2]
                temp3 = p0[-1] - p0[-2]
                R1 = r[r_ind][-1] + temp1*temp2/temp3
                if r_ind >= len(r) -1: R2 = rain_by_hour[i]
                else :
                    temp2 = r[r_ind + 1][-1] - r[r_ind+1][-2]
                    R2 = r[r_ind+1][-1] + temp1*temp2/temp3
            else:
                for k in range(0, len(r[0]) -1):
                    if p0[k] <= rain_by_hour <= p0[k+1]:
                        temp1 = rain_by_hour[i] - p0[-1]
                        temp2 = r[r_ind][k + 1] - r[r_ind][k]
                        temp3 = p0[k+1] - p0[k]
                        R1 = r[r_ind][k] + temp1*temp2/temp3
                        if r_ind >= len(r) - 1: R2 = rain_by_hour[i]
                        else :
                            temp2 = r[r_ind+1][k+1] - r[r_ind+1][k]
                            R2 = r[r_ind+1][k] + temp1*temp2/temp3
                            break
            if r_ind < len(r)-1:
                runoff[i] = (R2 - R1)/(w0[r_ind+1] - w0[r_ind])*(pa - w0[r_ind]) + R1
            else:
                runoff[i] = (R2 - R1)/(init_loss - w0[r_ind])*(pa - w0[r_ind]) + R1
        if model != 1:
            pa = pow(pa_decay_coe, dt/24.0)*(pa + rain_by_hour[i])
            if pa > init_loss: pa = init_loss

    if model == 1:  # 按累积降雨计算
        for i in range(len(rain_by_hour), 0, -1):
            runoff[i] = runoff[i] - runoff[i-1]
    return runoff






