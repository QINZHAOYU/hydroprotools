from data_visualize import data_visualize
import unittest


@unittest.skip("reason: show skip class")
class DemoTest(unittest.TestCase):
    def _demo(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())
        self.assertEqual('foo'.upper(), 'FOO')
        s = 'hello world'
        with self.assertRaises(TypeError):
            s.split(2)

    @unittest.skip("reasion: show skipping")
    def _demo2(self):
        # 跳过被此装饰器装饰的测， reason 为测试被跳过的原因。
        self.fail("should't happen")

    @unittest.skipIf('FOO'.isupper(), "reason:'foo' is upper already")
    def _demo3(self):
        # 当 condition 为真时，跳过被装饰的测试。
        self.assertTrue('FOO'.isupper())

    @unittest.skipUnless('foo'.isupper(), "reason:show this function")
    def _demo4(self):
        # 跳过被装饰的测试，除非 condition 为真。
        self.assertFalse('foo'.isupper())

    @unittest.expectedFailure
    def _demo5(self):
        # 把测试标记为预计失败。若测试不通过，会认为测试成功；若通过了，则认为测试失败。
        self.assertFalse('foo'.isupper())


class Pyecharts3DTest(unittest.TestCase):
    def setUp(self):
        self.chart = data_visualize.Pyecharts3D()
        self.data = []

    def tearDown(self):
        print("pyecharts3d test is done.")

    def test_scatter3d(self):
        pass


class MatPlotlibTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_lines(self):
        pass


def suite():
    suite = unittest.TestSuite()
    suite.addTest(Pyecharts3DTest('test_scatter3d'))
    suite.addTest(MatPlotlibTest('test_lines'))
    return suite


if __name__ == "__main__":
    runner = unittest.TestRunner()
    runner.run(suite())


# if __name__ == "__main__":
#     unittest.main()

