import os


"""
----------------------
python 异常类型
----------------------

BaseException	所有异常的基类
SystemExit	解释器请求退出
KeyboardInterrupt	用户中断执行(通常是输入^C)
Exception	常规错误的基类
StopIteration	迭代器没有更多的值
GeneratorExit	生成器(generator)发生异常来通知退出
StandardError	所有的内建标准异常的基类
ArithmeticError	所有数值计算错误的基类
FloatingPointError	浮点计算错误
OverflowError	数值运算超出最大限制
ZeroDivisionError	除(或取模)零 (所有数据类型)
AssertionError	断言语句失败
AttributeError	对象没有这个属性
EOFError	没有内建输入,到达EOF 标记
EnvironmentError	操作系统错误的基类
IOError	输入/输出操作失败
OSError	操作系统错误
WindowsError	系统调用失败
ImportError	导入模块/对象失败
LookupError	无效数据查询的基类
IndexError	序列中没有此索引(index)
KeyError	映射中没有这个键
MemoryError	内存溢出错误(对于Python 解释器不是致命的)
NameError	未声明/初始化对象 (没有属性)
UnboundLocalError	访问未初始化的本地变量
ReferenceError	弱引用(Weak reference)试图访问已经垃圾回收了的对象
RuntimeError	一般的运行时错误
NotImplementedError	尚未实现的方法
SyntaxError	Python 语法错误
IndentationError	缩进错误
TabError	Tab 和空格混用
SystemError	一般的解释器系统错误
TypeError	对类型无效的操作
ValueError	传入无效的参数
UnicodeError	Unicode 相关的错误
UnicodeDecodeError	Unicode 解码时的错误
UnicodeEncodeError	Unicode 编码时错误
UnicodeTranslateError	Unicode 转换时错误
Warning	警告的基类
DeprecationWarning	关于被弃用的特征的警告
FutureWarning	关于构造将来语义会有改变的警告
OverflowWarning	旧的关于自动提升为长整型(long)的警告
PendingDeprecationWarning	关于特性将会被废弃的警告
RuntimeWarning	可疑的运行时行为(runtime behavior)的警告
SyntaxWarning	可疑的语法的警告
UserWarning	用户代码生成的警告
"""


class FileFolderHandler(object):
    def __init__(self):
        pass

    @staticmethod
    def get_current_path():
        # 返回当前执行目录的绝对路径

        return os.getcwd()

    @staticmethod
    def is_path_exist(path):
        return os.path.exists(path)

    @staticmethod
    def is_path_abs(path):
        return os.path.isabs(path)

    @staticmethod
    def is_file(path):
        return os.path.isfile(path)

    @staticmethod
    def is_dir(path):
        return os.path.isdir(path)

    @staticmethod
    def split_dir_file(path):
        # 分离扩展名：
        # 若是目录，返回路径和空字符('E:\\Folder', '')；
        # 若是文件路径，返回文件路径和扩展名（'E:\\Folder\\temp', '.txt'）
        return os.path.splitext(path)

    @staticmethod
    def join_dir_file(path, file_name):
        # 路径拼接，拼接的路径不一定存在
        # 若路径不存在，返回file_name
        return os.path.join(path, file_name)

    @staticmethod
    def create_dir(folder_name):
        # 在当前目录下创建子文件夹
        os.mkdir(folder_name)

    @staticmethod
    def rename(old_name, new_name):
        # 对当前目录下的文件文件夹重命名
        os.rename(old_name, new_name)

    @staticmethod
    def get_dir_name(path):
        # 返回父目录绝对路径
        return os.path.dirname(path)

    @staticmethod
    def get_base_name(path):
        # 返回文件全名或最后一级文件夹名称
        return os.path.basename(path)

    @staticmethod
    def get_size(path):
        # 返回文件或文件夹的大小
        return os.path.getsize(path)

    @staticmethod
    def get_all_dirs_files(path):
        # 返回路径的当前路径，所有子目录，所有包含的文件（只包括当前目录下）
        roots, sub_dirs, files = [], [], []
        for base_path, folder_list, file_list in os.walk(path):
            roots = base_path
            sub_dirs = folder_list
            files = file_list
            break
        return roots, sub_dirs, files


class DataToFile(object):
    def __init__(self):
        pass

    @staticmethod
    def write_list(self, out_path, out_file):
        path = FileFolderHandler.join_dir_file(out_path, out_file)
        if not FileFolderHandler.is_file(path):
            raise ValueError("out_path+out_file not point to  a file")

        file = open(path, "w")
        for data in self.data:
            for i in range(0, int(self.num), 1):
                file.write(str(data[i]) + "\t")
            file.write("\n")
        file.close()


class FileToData(object):
    def __init__(self):
        pass

    @staticmethod
    def read_data_all(path):
        if not FileFolderHandler.is_file(path):
            raise ValueError("path not point to a file", path)

        file = open(path, "r")
        data = file.readlines()
        file.close()
        return data

    @staticmethod
    def read_data_yield(path, MAX_SIXE=1024):
        if not FileFolderHandler.is_file(path):
            raise ValueError("path not point to a file", path)

        with open(path, "r") as file:
            while True:
                block = file.read(MAX_SIXE)
                if block:
                    yield block
                else:
                    return

    @staticmethod
    def split_data_line(data_line, delim=','):
        arr = data_line.strip().split(delim)
        return arr


