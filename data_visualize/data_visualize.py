from enum import Enum, unique
import math
import random
import datetime

import asyncio
from aiohttp import TCPConnector, ClientSession

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

from pyecharts.charts import Surface3D, Scatter3D, Bar3D, Line3D
from pyecharts.charts import Calendar, Gauge, Graph
from pyecharts.faker import Faker
from pyecharts import options as opts

import toolkits.filetools as tool
import data_set.data_mysql as sql


class Pyecharts3D(object):
    @unique
    class ChartType(Enum):
        # python 枚举类；unique保证每个枚举量都是唯一的值。
        BAR3D = 1
        LINE3D = 2
        SCATTER3D = 3
        SURFACE3D = 4
        MAP3D = 5

    def __init__(self):
        self.type = Pyecharts3D.ChartType

    def _demo_scatter3d(self):
        # 见project_cfd.py.
        pass

    def _demo_bar3d(self):
        def data_process(f_path):
            data = []
            for line in tool.FileToData.read_data_yield(f_path):
                arr = tool.FileToData.split_data_line(line)
                for it in arr:  # 有的文件中只有一行，但是数据间还有\n
                    data.append(it.strip())
            return data

        database = sql.PyechartBar3dData()
        data_table = database.out_data()
        hours_f, days_f, data_f = data_table[0][1], data_table[0][2], data_table[0][3]

        hours = data_process(hours_f)
        days = data_process(days_f)
        data = data_process(data_f)
        data = [data[ind] for ind in range(0, len(data), 1) if data[ind] is not ""]

        data_restore = []
        for it in range(0, len(data), 3):
            val1 = data[it].split('[')[1]
            val2 = data[it+1]
            val3 = data[it+2].split(']')[0]
            data_restore.append([int(val2), int(val1), int(val3)])

        (
            Bar3D(init_opts=opts.InitOpts(width="1000px", height="800px"))
            .add(
                series_name="bar3d_demo",
                data=data_restore,
                xaxis3d_opts=opts.Axis3DOpts(type_="category", data=hours),
                yaxis3d_opts=opts.Axis3DOpts(type_="category", data=days),
                zaxis3d_opts=opts.Axis3DOpts(type_="value")
            )
            .set_global_opts(visualmap_opts=opts.VisualMapOpts(
                max_=20,
                range_color=[
                    "#313695",
                    "#4575b4",
                    "#74add1",
                    "#abd9e9",
                    "#e0f3f8",
                    "#ffffbf",
                    "#fee090",
                    "#fdae61",
                    "#f46d43",
                    "#d73027",
                    "#a50026"]
            ))
        ).render("bar3d_demo.html")

    def _demo_line3d(self):
        week_en = "Saturday Friday Thursday Wednesday Tuesday Monday Sunday".split()
        clock = (
            "12a 1a 2a 3a 4a 5a 6a 7a 8a 9a 10a 11a 12p "
            "1p 2p 3p 4p 5p 6p 7p 8p 9p 10p 11p".split()
        )
        data = []
        for t in range(0, 25000):
            _t = t/1000
            x = (1+0.25*math.cos(75*_t))*math.cos(_t)
            y = (1+0.25*math.cos(75*_t))*math.sin(_t)
            z = _t + 2.0*math.sin(75*_t)
            data.append([x, y, z])

        (
            Line3D(init_opts=opts.InitOpts(width="1000px", height="800px"))
            .add(
                series_name="line3d_demo",
                data=data,
                xaxis3d_opts=opts.Axis3DOpts(type_="value", data=clock),
                yaxis3d_opts=opts.Axis3DOpts(type_="value", data=week_en),
                grid3d_opts=opts.Grid3DOpts(width=200, height=200, depth=200)
            )
            .set_global_opts(visualmap_opts=opts.VisualMapOpts(
                dimension=2,
                max_=30,
                min_=0,
                range_color=[
                    "#313695",
                    "#4575b4",
                    "#74add1",
                    "#abd9e9",
                    "#e0f3f8",
                    "#ffffbf",
                    "#fee090",
                    "#fdae61",
                    "#f46d43",
                    "#d73027",
                    "#a50026"]
            ))
        ).render("line3d_demo.html")

    def _demo_surface3d(self):
        def data_process():
            for t0 in np.arange(-3, 3, 0.05):
                y = t0
                for t1 in np.arange(-3, 3, 0.05):
                    x = t1
                    z = math.sin(x**2 + y**2)*x/math.pi
                    yield [x, y, z]

        (
            Surface3D(init_opts=opts.InitOpts(width="1000px", height="800px"))
            .add(
                series_name="surface3d_demo",
                data=list(data_process()),
                shading="color",
                xaxis3d_opts=opts.Axis3DOpts(type_="value"),
                yaxis3d_opts=opts.Axis3DOpts(type_="value"),
                grid3d_opts=opts.Grid3DOpts(width=200, height=100, depth=200)
            )
            .set_global_opts(visualmap_opts=opts.VisualMapOpts(
                dimension=2,
                max_=1,
                min_=-1,
                range_color=[
                    "#313695",
                    "#4575b4",
                    "#74add1",
                    "#abd9e9",
                    "#e0f3f8",
                    "#ffffbf",
                    "#fee090",
                    "#fdae61",
                    "#f46d43",
                    "#d73027",
                    "#a50026"]
            ))
        ).render("surface3d_demo.html")

    def _demo_map3d(self):
        # 效果一般，而且基本用不上。
        pass

    def run_demo(self, Type):
        if self.type[Type]   == self.type.BAR3D:
            self._demo_bar3d()
        elif self.type[Type] == self.type.LINE3D:
            self._demo_line3d()
        elif self.type[Type] == self.type.SCATTER3D:
            self._demo_scatter3d()
        elif self.type[Type] == self.type.SURFACE3D:
            self._demo_surface3d()
        elif self.type[Type] == self.type.MAP3D:
            self._demo_map3d()
        else:
            raise ValueError("not valid type", Type)

    @staticmethod
    def scatter3d(z_mat, name) -> Scatter3D:
        chart = (
            Scatter3D(init_opts=opts.InitOpts(width="1000px", height="800px"))
            .add(
                series_name=name,
                data=z_mat,
                xaxis3d_opts=opts.Axis3DOpts(type_="value"),
                yaxis3d_opts=opts.Axis3DOpts(type_="value"),
                zaxis3d_opts=opts.Axis3DOpts(type_="value"),
                grid3d_opts=opts.Grid3DOpts(width=200, height=200, depth=200)
            )
            .set_global_opts(visualmap_opts=opts.VisualMapOpts(
                type_="color",
                dimension=3,
                range_color=[
                    "#313695",
                    "#4575b4",
                    "#74add1",
                    "#abd9e9",
                    "#e0f3f8",
                    "#ffffbf",
                    "#fee090",
                    "#fdae61",
                    "#f46d43",
                    "#d73027",
                    "#a50026"]
            ))
        )
        return chart

    @staticmethod
    def scatter3d_multi(data1, data2) -> Scatter3D:
        z_mat1, name1 = data1[0], data1[1]
        z_mat2, name2 = data2[0], data2[1]

        chart = (
            Scatter3D(init_opts=opts.InitOpts(width="1000px", height="800px"))
            .add(
                series_name=name1,
                data=z_mat1,
                xaxis3d_opts=opts.Axis3DOpts(type_="value"),
                yaxis3d_opts=opts.Axis3DOpts(type_="value"),
                zaxis3d_opts=opts.Axis3DOpts(type_="value"),
                grid3d_opts=opts.Grid3DOpts(width=200, height=200, depth=200)
            )
            .add(
                series_name=name2,
                data=z_mat2,
                xaxis3d_opts=opts.Axis3DOpts(type_="value"),
                yaxis3d_opts=opts.Axis3DOpts(type_="value"),
                zaxis3d_opts=opts.Axis3DOpts(type_="value"),
                grid3d_opts=opts.Grid3DOpts(width=200, height=200, depth=200)
            )
            .set_global_opts(visualmap_opts=opts.VisualMapOpts(
                type_="color",
                dimension=3,
                range_color=[
                    "#313695",
                    "#4575b4",
                    "#74add1",
                    "#abd9e9",
                    "#e0f3f8",
                    "#ffffbf",
                    "#fee090",
                    "#fdae61",
                    "#f46d43",
                    "#d73027",
                    "#a50026"]
            ))
        )
        return chart

    @staticmethod
    def bar3d(x_arr, y_arr, z_mat, name) -> Bar3D:
        z_arr = [z_mat[i][2] for i in range(0, len(z_mat), 1)]

        chart = (
            Bar3D(init_opts=opts.InitOpts(width="1000px", height="800px"))
            .add(
                series_name=name,
                data=z_mat,
                xaxis3d_opts=opts.Axis3DOpts(type_="category", data=x_arr),
                yaxis3d_opts=opts.Axis3DOpts(type_="category", data=y_arr),
                zaxis3d_opts=opts.Axis3DOpts(type_="value")
            )
            .set_global_opts(visualmap_opts=opts.VisualMapOpts(
                max_=max(z_arr),
                range_color=[
                    "#313695",
                    "#4575b4",
                    "#74add1",
                    "#abd9e9",
                    "#e0f3f8",
                    "#ffffbf",
                    "#fee090",
                    "#fdae61",
                    "#f46d43",
                    "#d73027",
                    "#a50026"]
            ))
        )
        return chart

    @staticmethod
    def bar3d_multi(data1, data2) -> Bar3D:
        x1, y1, zmat1, name1 = data1[0], data1[1], data1[2], data1[3]
        x2, y2, zmat2, name2 = data2[0], data2[1], data2[2], data2[3]

        chart = (
            Bar3D(init_opts=opts.InitOpts(width="1000px", height="800px"))
            .add(
                series_name=name1,
                data=zmat1,
                xaxis3d_opts=opts.Axis3DOpts(type_="category", data=x1),
                yaxis3d_opts=opts.Axis3DOpts(type_="category", data=y1),
                zaxis3d_opts=opts.Axis3DOpts(type_="value")
            )
            .add(
                series_name=name2,
                data=zmat2,
                xaxis3d_opts=opts.Axis3DOpts(type_="category", data=x2),
                yaxis3d_opts=opts.Axis3DOpts(type_="category", data=y2),
                zaxis3d_opts=opts.Axis3DOpts(type_="value")
            )
            .set_global_opts(visualmap_opts=opts.VisualMapOpts(
                range_color=[
                    "#313695",
                    "#4575b4",
                    "#74add1",
                    "#abd9e9",
                    "#e0f3f8",
                    "#ffffbf",
                    "#fee090",
                    "#fdae61",
                    "#f46d43",
                    "#d73027",
                    "#a50026"]
            ))
        )
        return chart

    @staticmethod
    def line3d(x_arr, y_arr, z_mat, name) -> Line3D:
        z_arr = [z_mat[i][2] for i in range(0, len(z_mat), 1)]
        chart = (
            Line3D(init_opts=opts.InitOpts(width="1000px", height="800px"))
            .add(
                series_name=name,
                data=z_mat,
                xaxis3d_opts=opts.Axis3DOpts(type_="value", data=x_arr),
                yaxis3d_opts=opts.Axis3DOpts(type_="value", data=y_arr),
                grid3d_opts=opts.Grid3DOpts(width=200, height=200, depth=200)
            )
            .set_global_opts(visualmap_opts=opts.VisualMapOpts(
                max_=max(z_arr),
                min_=min(z_arr),
                range_color=[
                    "#313695",
                    "#4575b4",
                    "#74add1",
                    "#abd9e9",
                    "#e0f3f8",
                    "#ffffbf",
                    "#fee090",
                    "#fdae61",
                    "#f46d43",
                    "#d73027",
                    "#a50026"]
            ))
        )
        return chart

    @staticmethod
    def line3d_multi(data1, data2) -> Line3D:
        x1, y1, zmat1, name1 = data1[0], data1[1], data1[2], data1[3]
        x2, y2, zmat2, name2 = data2[0], data2[1], data2[2], data2[3]

        chart = (
            Line3D(init_opts=opts.InitOpts(width="1000px", height="800px"))
            .add(
                series_name=name1,
                data=zmat1,
                xaxis3d_opts=opts.Axis3DOpts(type_="value", data=x1),
                yaxis3d_opts=opts.Axis3DOpts(type_="value", data=y1),
                grid3d_opts=opts.Grid3DOpts(width=200, height=200, depth=200)
            )
            .add(
                series_name=name2,
                data=zmat2,
                xaxis3d_opts=opts.Axis3DOpts(type_="value", data=x2),
                yaxis3d_opts=opts.Axis3DOpts(type_="value", data=y2),
                grid3d_opts=opts.Grid3DOpts(width=200, height=200, depth=200)
            )
            .set_global_opts(visualmap_opts=opts.VisualMapOpts(
                range_color=[
                    "#313695",
                    "#4575b4",
                    "#74add1",
                    "#abd9e9",
                    "#e0f3f8",
                    "#ffffbf",
                    "#fee090",
                    "#fdae61",
                    "#f46d43",
                    "#d73027",
                    "#a50026"]
            ))
        )
        return chart

    @staticmethod
    def surface3d(z_mat, name) -> Surface3D:
        z_arr = [z_mat[i][2] for i in range(0, len(z_mat), 1)]

        chart = (
            Surface3D(init_opts=opts.InitOpts(width="1000px", height="800px"))
            .add(
                series_name=name,
                shading="color",
                data=z_mat,
                xaxis3d_opts=opts.Axis3DOpts(type_="value"),
                yaxis3d_opts=opts.Axis3DOpts(type_="value"),
                grid3d_opts=opts.Grid3DOpts(width=200, height=150, depth=200)
            )
            .set_global_opts(visualmap_opts=opts.VisualMapOpts(
                max_=max(z_arr),
                min_=min(z_arr),
                range_color=[
                    "#313695",
                    "#4575b4",
                    "#74add1",
                    "#abd9e9",
                    "#e0f3f8",
                    "#ffffbf",
                    "#fee090",
                    "#fdae61",
                    "#f46d43",
                    "#d73027",
                    "#a50026"]
            ))
        )
        return chart

    @staticmethod
    def surface3d_multi(data1, data2) -> Surface3D:
        z_mat1, name1 = data1[0], data1[1]
        z_mat2, name2 = data2[0], data2[1]

        chart = (
            Surface3D(init_opts=opts.InitOpts(width="1000px", height="800px"))
            .add(
                series_name=name1,
                data=z_mat1,
                shading="color",
                xaxis3d_opts=opts.Axis3DOpts(type_="value"),
                yaxis3d_opts=opts.Axis3DOpts(type_="value"),
                grid3d_opts=opts.Grid3DOpts(width=200, height=150, depth=200)
            )
            .add(
                series_name=name2,
                data=z_mat2,
                shading="color",
                xaxis3d_opts=opts.Axis3DOpts(type_="value"),
                yaxis3d_opts=opts.Axis3DOpts(type_="value"),
                grid3d_opts=opts.Grid3DOpts(width=200, height=150, depth=200)
            )
            .set_global_opts(visualmap_opts=opts.VisualMapOpts(
                range_color=[
                    "#313695",
                    "#4575b4",
                    "#74add1",
                    "#abd9e9",
                    "#e0f3f8",
                    "#ffffbf",
                    "#fee090",
                    "#fdae61",
                    "#f46d43",
                    "#d73027",
                    "#a50026"]
            ))
        )
        return chart


# demo = Pyecharts3D()
# demo.run_demo('BAR3D')
# demo.run_demo('LINE3D')
# demo.run_demo('SURFACE3D')


class PyechartBase(object):
    class ChartType(Enum):
        CALENDAR = 1
        GAUGE = 2
        GRAPH = 3

    def __init__(self):
        self.type = PyechartBase.ChartType

    def _demo_calendar(self):
        begin = datetime.date(2020, 1, 1)
        end = datetime.date(2020, 3, 23)
        data = [
            [str(begin + datetime.timedelta(days=i)), random.randint(1000, 25000)]
            for i in range((end-begin).days+1)
        ]

        (
            Calendar(init_opts=opts.InitOpts(width="1000px", height="800px"))
            .add(
                series_name="calendar_demo",
                yaxis_data=data,
                calendar_opts=opts.CalendarOpts(
                    pos_top="120",
                    pos_left="30",
                    pos_right="30",
                    range_="2020",
                    yearlabel_opts=opts.CalendarYearLabelOpts(is_show=False)
                )
            )
            .set_global_opts(
                title_opts=opts.TitleOpts(
                    pos_top="30",
                    pos_left="center",
                    title="2020微信步数"),
                visualmap_opts=opts.VisualMapOpts(
                    max_=20000, min_=500, orient="horizontal", is_piecewise=False)
            )
            .render("calendar_demo.html")
        )

    def _demo_gauge(self):
        (
            Gauge(init_opts=opts.InitOpts(width="1000px", height="800px"))
            .add(
                series_name="guage_demo",
                data_pair=[["完成率", 64.6]]
            )
            .set_global_opts(
                legend_opts=opts.LegendOpts(is_show=False),
                tooltip_opts=opts.TooltipOpts(is_show=True, formatter="{a}<br/>{br}:{c}%")
            )
            .set_series_opts(axisline_opts=opts.AxisLineOpts(
                linestyle_opts=opts.LineStyleOpts(
                    color=[[0.3, "#67e0e3"], [0.7, "#37a2da"], [1, "#fd666d"]],
                    width=30))
            )
            .render("gauge_demo.html")
        )

    def _demo_graph(self):
        async def get_json_data(url:str) -> dict:
            async with ClientSession(connector=TCPConnector(ssl=False)) as session:
                async with session.get(url=url) as respone:
                    return await respone.json()

        url_str = "https://echarts.baidu.com/examples/data/asset/data/npmdepgraph.min10.json"
        data = asyncio.run(get_json_data(url_str))
        nodes = [
            {
                "x": node["x"],
                "y": node["y"],
                "id": node["id"],
                "name": node["label"],
                "symbolSize": node["size"],
                "itemStyle": {"normal": {"color": node["color"]}}
            }
            for node in data["nodes"]
        ]
        edges = [
            {"source": edge["sourceID"], "target": edge["targetID"]}
            for edge in data["edges"]
        ]
        # graph_data = sql.PyechartGraphData()
        # nodes = []
        # for node in graph_data.query_nodes_data():
        #     nodes.append(
        #         {"id": node[0], "x": node[1], "y": node[2],
        #          "name": node[3], "symbolSize": node[4], "itemStyle": node[5]}
        #     )
        # edges = []
        # for edge in graph_data.query_edges_data():
        #     edges.append({"source": edge[0], "target": edge[1]})

        (
            Graph(init_opts=opts.InitOpts(width="1000px", height="800px"))
            .add(
                series_name="",
                nodes=nodes,
                links=edges,
                layout="none",
                is_roam=True,
                is_focusnode=True,
                label_opts=opts.LabelOpts(is_show=False),
                linestyle_opts=opts.LineStyleOpts(width=0.5, curve=0.3, opacity=0.7)
            )
            .set_global_opts(title_opts=opts.TitleOpts(title="NPM Dependencies"))
            .render("graph_demo.html")
        )

    def run_demo(self, type):
        if self.type[type] == self.type.CALENDAR:
            self._demo_calendar()
        elif self.type[type] == self.type.GAUGE:
            self._demo_gauge()
        elif self.type[type] == self.type.GRAPH:
            self._demo_graph()

    @staticmethod
    def calendar(data, year, name) -> Calendar:
        z_arr = [data[i][1] for i in range(len(data))]

        chart = (
            Calendar(init_opts=opts.InitOpts(width="1000px", height="800px"))
            .add(
                series_name="",
                yaxis_data=data,
                calendar_opts=opts.CalendarOpts(
                    pos_top="120",
                    pos_left="30",
                    pos_right="30",
                    range_=year,
                    yearlabel_opts=opts.CalendarYearLabelOpts(is_show=False))
            )
            .set_global_opts(
                title_opts=opts.TitleOpts(
                    pos_top="30",
                    pos_left="center",
                    title=name),
                visualmap_opts=opts.VisualMapOpts(
                    max_=max(z_arr),
                    min_=min(z_arr),
                    orient="horizontal",
                    is_piecewise=False)
            )
        )
        return chart

    @staticmethod
    def guage(data, name, color_range):
        chart = (
            Gauge(init_opts=opts.InitOpts(width="1000px", height="800px"))
            .add(
                series_name=name,
                data_pair=data
            )
            .set_global_opts(
                legend_opts=opts.LegendOpts(is_show=False),
                tooltip_opts=opts.TooltipOpts(is_show=True, formatter="{a}<br/>{b}:{c}%")
            )
            .set_series_opts(
                axisline_opts=opts.AxisLineOpts(
                    linestyle_opts=opts.LineStyleOpts(color=color_range, width=30))
            )
        )
        return chart


# demo = PyechartBase()
# demo.run_demo("CALENDAR")
# demo.run_demo("GAUGE")
# demo.run_demo("GRAPH")


class PyechartMulti(object):
    def __init__(self):
        pass


class Matplotlib(object):
    def __init__(self):
        pass

    @staticmethod
    def lines(X, Y, name):
        pass





